import * as util from './util'
import * as events from 'events'

export abstract class Extractor {
  /**
  * An abstract function used to extract the name or name from tags information.
  * @param data is the row from ReadStream of a CSV file.
  * @return the name information.
  */
  abstract extract(data: any): any
}

class NameTagsExtractor extends Extractor {
  /**
  * An extract function defintion where name or tags are extracted.
  * @param data is the row from ReadStream of a CSV file.
  * @return the name information.
  */
  extract (data: any) {
    const name = data.name
    const tags = data.tags
    const tagsName = this.tagsName(name, tags)

    if (tagsName !== '') {
      return tagsName
    } else {
      return name
    }
  }

  /**
  * An extract the name information from the tags column.
  * @param name is the name column value.
  * @param tags is the tags column value.
  * @return the name or list of names information.
  */
  private tagsName (name: string, tags: string) {
    if (tags && tags.includes('=')) {
      const tagValues = tags.replace('{', '').replace('}', '')
      const tagNames = tagValues.split(',').map(item => {
        const [key, value] = item.split('=')
        if (key.startsWith('name') || key.startsWith('name:en') || key.startsWith('name:right') || key.startsWith('name:left')) {
          return value
        } else {
          return ''
        }
      }).filter(item => item !== '')

      if (tagNames.length === 1) {
        if (tagNames[0] === name) {
          return name
        } else if (name.length === 0) {
          return tagNames[0]
        }
      } else if (tagNames.length > 1) {
        return tagNames
      }
    } else {
      return ''
    }
  }
}

class PoiMatchingExtractor extends Extractor {
  /**
  * An extract function defintion that creates a JSON object based on the data param.
  * @param data is the row from ReadStream of a CSV file.
  * @return JSON object.
  */
  extract (data: any) {
    return { osm_type: data.osm_type, osm_id: data.osm_id, internal_id: data.internal_id, query: data.query }
  }
}

const osmPoiSet = new Set<String>()
const googlePoiSet = new Set<String>()
const poiMatchingSet: any[] = []

const eventEmitter = new events.EventEmitter()
const nameTagsExtractor = new NameTagsExtractor()

const OSM_FILE:string = 'osm_poi.csv'
const GOOGLE_FILE:string = 'google_poi.csv'
const POI_MATCHING_FILE:string = 'google_osm_poi_matching.csv'
const CS_FILE:string = 'confidence_score.csv'

const PROCESSES:[string, Set<String> | Array<String>, Extractor][] = [
  [OSM_FILE, osmPoiSet, nameTagsExtractor],
  [GOOGLE_FILE, googlePoiSet, nameTagsExtractor],
  [POI_MATCHING_FILE, poiMatchingSet, new PoiMatchingExtractor()]
]

eventEmitter.once(OSM_FILE, () => {
  eventEmitter.emit('score')
})

eventEmitter.once(GOOGLE_FILE, () => {
  eventEmitter.emit('score')
})

eventEmitter.once(POI_MATCHING_FILE, () => {
  eventEmitter.emit('score')
})

eventEmitter.on('score', () => {
  if (eventEmitter.eventNames().length === 1) {
    for (const columns of poiMatchingSet) {
      const query = columns.query.toLowerCase()
      if (osmPoiSet.has(query) && googlePoiSet.has(query)) {
        columns.score = 1
      } else {
        columns.score = 0
      }
    }

    console.log('Confidence scoring completed.')
    util.writeCSVFile(CS_FILE, poiMatchingSet).then(() => {
      console.log(`Confidence score was successfully saved, new file: ${CS_FILE} was created.`)
    })
  }
})

const readRowFn = (row: any, storage: Set<String> | Array<String>, extractor: Extractor) => {
  if (storage instanceof Set) {
    const name = extractor.extract(row)
    if (typeof name === 'string') {
      storage.add(name.toLowerCase())
    } else if (Array.isArray(name)) {
      name.forEach(storage.add, storage)
    }
  } else {
    storage.push(extractor.extract(row))
  }
}

for (const [file, poi, extractor] of PROCESSES) {
  util.readCSVFile(file, poi, eventEmitter, extractor, readRowFn).then(() => {
    console.log(`File read ${file} completed.`)
  })
}
