import * as fs from 'fs'
import * as csvParser from 'csv-parser'
import * as csvWriter from 'fast-csv'
import { EventEmitter } from 'events'
import { Extractor } from './app'

/**
 * Reads a CSV file.
 * @param fileName is the file name of the CSV file.
 * @param storage is a Set where the data are saved.
 * @param eventEmitter is an instance of events.EventEmitter.
 * @param extractor is an object where the data extractor is defined.
 * @param readRowFn is a function that takes an argument for each data read in a row.
 * @param separator is a CSV separator, default ';'.
 */
export async function readCSVFile (
  fileName: string,
  storage: Set<String> | Array<String>,
  eventEmitter: EventEmitter,
  extractor: Extractor,
  readRowFn: (row: any, storage: Set<String> | Array<String>, Extractor) => void,
  separator: string = ';'
) {
  fs.createReadStream(fileName)
    .pipe(csvParser({ separator: separator }))
    .on('data', (row) => { readRowFn(row, storage, extractor) })
    .on('end', () => {
      eventEmitter.emit(fileName)
    })
}

/**
 * Writes a CSV file.
 * @param fileName is the CSV file.
 * @param data is the csv formatted data to save.
 */
export async function writeCSVFile (fileName: string, data: any) {
  csvWriter
    .write(data, { headers: true })
    .pipe(fs.createWriteStream(fileName))
}
